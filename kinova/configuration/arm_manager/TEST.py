from kinova.configuration.arm_manager.arm_manager import ArmManager
from typing import List
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm
from hein_robots.kinova.kinova_sequence import KinovaSequence
from kinova.configuration.sequences.sequences import sequences, sequence_names

arm = KinovaGen3Arm()
print('arm_connected')
arm_manager = ArmManager(arm = arm, action_sequences = sequences, sequence_names = sequence_names)
print('configuration_done')
arm_manager.vial_hplc_exchange()