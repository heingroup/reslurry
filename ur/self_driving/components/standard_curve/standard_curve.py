from ur.components.vial_handling.vial_handling import VialHandling
from ur.components.hplc_comm.hplc_comm import HPLC_Comm
from ur.components.hplc_handling.hplc_handling import HplcHandling
from ur.components.samplomatic_handling.samplomatic_handling import SamplomaticHandling
from ur.components.samplomatic.samplomatic import Samplomatic
from ika import Thermoshaker
from ur.components.data_handling.data_handling import DataHandling
import time
from ur.configuration import deck_consumables
import pandas as pd
import json
from datetime import datetime
import os
import matplotlib.pyplot as plt
from scipy import stats






class StandardCurve:
    def __init__(self, solid_name, hplc_runtime: float, output_json: str, output_folder: str, average_output_json: str, vial_handling: VialHandling, hplc_handling: HplcHandling,
                 hplc_comm : HPLC_Comm, samplomatic : Samplomatic, somatic : SamplomaticHandling, ts : Thermoshaker,
                 data_handling: DataHandling):
        self.solid_name = solid_name
        self.hplc_runtime = hplc_runtime
        self.output_json = output_json
        self.output_folder = output_folder
        self.average_output_json = average_output_json
        self.vial_handling = vial_handling
        self.hplc_comm = hplc_comm
        self.hplc_handling = hplc_handling
        self.samplomatic = samplomatic
        self.somatic = somatic
        self.ts = ts
        self.data_handling = data_handling

    def run_standard_curve(self, predosed_vial_index, shaker_index, mass, prime_volume_ml,
                           temp, temp_time_min, initial_volume_ml, sample_volume_ml,
                           dilution_factor, number_of_dilutions, skip_to_data_processing: bool = False):
        if not skip_to_data_processing:
            # Error checking
            if dilution_factor * sample_volume_ml > 1.5:
                print('Volume exceeded HPLC vial limit, check values')
                return
            if (dilution_factor * sample_volume_ml) - sample_volume_ml < 0.5:
                print('Volume too low, check values')
                return
            vial_list = []
            # Tells HPLC to set up a standard curve queue with X number of samples
            self.hplc_comm.send_tcp_message('standard_curve' + '_' + str(number_of_dilutions + 1))
            # Put solvent in vial
            vial_index = predosed_vial_index
            vial_list.append(vial_index)
            self.samplomatic.prime(volume_ml=prime_volume_ml)
            self.somatic.grab_samplomatic()
            self.somatic.dispense_hplc_grid(vial_index=vial_index, volume_ml=initial_volume_ml)
            self.somatic.return_samplomatic()
            # Shake it for X time to make everything dissolve
            self.vial_handling.vial_from_tray(vial_index=vial_index)
            self.vial_handling.cup_to_shaker(shaker_index=shaker_index, vial_type='hplc')
            self.vial_handling.home()
            self.ts.set_speed = 600
            self.ts.start_shaking()
            self.ts.set_temperature = temp
            time.sleep(1)
            self.ts.start_tempering()
            time.sleep(temp_time_min * 60)
            self.ts.stop_shaking()
            self.vial_handling.vial_from_shaker(shaker_index=shaker_index)
            self.vial_handling.vial_to_tray(vial_index=vial_index)
            # Draw sample from vial it just made, dispense with backend solvent to dilute into a new vial
            for n in range(number_of_dilutions):
                self.samplomatic.prime(volume_ml=0.2)
                self.somatic.grab_samplomatic()
                self.somatic.draw_sample_from_vial(volume_ml=sample_volume_ml, sample_index=vial_index)
                vial_index = deck_consumables.get_clean_vial(insert=False)[0]
                vial_list.append(vial_index)
                self.somatic.dispense_hplc_grid(vial_index=vial_index, volume_ml=sample_volume_ml * dilution_factor)
                self.somatic.return_samplomatic()

            self.hplc_handling.run_blank()
            for vial_index in vial_list:
                self.hplc_handling.run_current_vial(vial_index=vial_index, hplc_runtime=self.hplc_runtime)
            self.hplc_handling.return_last_vial()
            self.hplc_handling.process_data_and_send_csv()
            time.sleep(15)
        hplc_file = self.data_handling.find_most_recent_hplc_data()
        self.make_standard_curve(hplc_file=hplc_file, number_of_samples=number_of_dilutions + 1,
                                          initial_mass=mass, initial_volume=initial_volume_ml,
                                          dilution_factor=dilution_factor,
                                          min_r2=0.99)

    def make_standard_curve(self, hplc_file, number_of_samples, initial_mass, initial_volume, dilution_factor,
                            min_r2):
        # Read the CSV file into a pandas DataFrame, excluding the first column
        df = pd.read_csv(hplc_file, index_col="sample")

        # Get the column names excluding the first column
        columns = df.columns[1:]

        # Dictionary to store the results
        results_dict = {
            'date': pd.Timestamp.now().isoformat(),
            'compound_name': self.solid_name,
            'dilution_factor': dilution_factor,
            'peaks': {}
        }

        concentrations = [initial_mass / initial_volume]
        for _ in range(1, number_of_samples):
            concentrations.append(concentrations[-1] / dilution_factor)
        concentrations_copy = concentrations

        for column in columns:
            first_run_removed = False
            concentrations_copy = concentrations.copy()  # Make a copy of the concentrations list

            # Select the column of interest
            compound_data = df[column].values
            compound_data_copy = compound_data.copy()  # Make a copy of the compound_data array

            # Check for zeros in compound_data
            zeros_mask = compound_data_copy == 0

            # Exclude zero values from compound_data_copy and concentrations_copy
            compound_data_copy = compound_data_copy[~zeros_mask]
            concentrations_copy = [c for i, c in enumerate(concentrations_copy) if not zeros_mask[i]]

            #If there was no data or peak areas, aquired, this will allow the script to continue instead of erroring
            if not concentrations_copy or not compound_data_copy.any():
                slope, intercept, r_value = (0,0,0)

            else:
            # Perform linear regression using scipy.stats
                slope, intercept, r_value, _, _ = stats.linregress(concentrations_copy, compound_data_copy)

            # Calculate R-squared value
            r2 = r_value ** 2

            # if (r2 < min_r2):
            #     first_run_removed = True
            #
            #     # Perform linear regression using scipy.stats
            #     slope, intercept, r_value, _, _ = stats.linregress(concentrations_copy[1:], compound_data_copy[1:])
            #
            #     # Calculate R-squared value
            #     r2 = r_value ** 2

            # Create a dictionary for the result of the current peak label
            peak_dict = {
                'slope': slope,
                'intercept': intercept,
                'r2': r2,
                'first_run_removed': first_run_removed
            }

            # Add the peak dictionary to the results dictionary
            results_dict['peaks'][column] = peak_dict

        # Read the previous runs from the existing JSON file
        with open(self.output_json, 'r') as json_file:
            previous_runs = json.load(json_file)

        # Append the current run dictionary to the list of previous runs
        previous_runs.append(results_dict)

        # Save the combined results to the output JSON file
        with open(self.output_json, 'w') as json_file:
            json.dump(previous_runs, json_file)

        # Save the raw data to a CSV file
        current_date = datetime.now().strftime('%m-%d-%Y')
        output_folder =os.path.join(self.output_folder, current_date)
        os.makedirs(output_folder, exist_ok=True)
        file_counter = 1
        while True:
            output_file = os.path.join(output_folder,
                                       f'{self.solid_name}_dilution_{dilution_factor}_{file_counter}.csv')
            if not os.path.exists(output_file):
                break
            file_counter += 1

        # Save the raw data to a CSV file
        print(output_file)
        raw_data = pd.DataFrame({'Concentration': concentrations[:len(compound_data)], **df.iloc[:, 1:]})
        raw_data.to_csv(output_file, index=False)

    def check_r2_threshold(self, dilution_factor, threshold):
        # Read JSON data from file
        try:
            with open(self.output_json, 'r') as file:
                data = json.load(file)
        except FileNotFoundError:
            print("File not found")
            return False
        except json.JSONDecodeError as e:
            print("Error decoding JSON:", str(e))
            return False

        # Filter dictionaries based on dilution factor
        filtered_dicts = [d for d in data if d.get("dilution_factor") == dilution_factor]

        # Check if any dictionaries match the dilution factor
        if not filtered_dicts:
            print(f"No dictionaries found with dilution_factor: {dilution_factor}")
            return False

        # Get the most recent dictionary from the filtered list
        most_recent_dict = filtered_dicts[-1]

        # Check if the "peaks" key is present
        if "peaks" not in most_recent_dict:
            print("No 'peaks' key in the most recent dictionary")
            return False

        # Iterate over the "peaks" dictionary and check the "r2" values
        for peak, values in most_recent_dict["peaks"].items():
            if "r2" not in values:
                print(f"No 'r2' value for peak '{peak}'")
                return False

            r2 = values["r2"]
            if r2 < threshold:
                print(f"r2 value for peak '{peak}' at '{dilution_factor}' is below the threshold")
                return False

        # All "r2" values are above the threshold
        return True

    def check_intercept_through_zero(self, dilution_factor, percent_of_slope):
        # Read JSON data from file
        try:
            with open(self.output_json, 'r') as file:
                data = json.load(file)
        except FileNotFoundError:
            print("File not found")
            return False
        except json.JSONDecodeError as e:
            print("Error decoding JSON:", str(e))
            return False

        # Filter dictionaries based on dilution factor
        filtered_dicts = [d for d in data if d.get("dilution_factor") == dilution_factor]

        # Check if any dictionaries match the dilution factor
        if not filtered_dicts:
            print(f"No dictionaries found with dilution_factor: {dilution_factor}")
            return False

        # Get the most recent dictionary from the filtered list
        most_recent_dict = filtered_dicts[-1]

        # Check if the "peaks" key is present
        if "peaks" not in most_recent_dict:
            print("No 'peaks' key in the most recent dictionary")
            return False

        # Iterate over the "peaks" dictionary and check the "slope" and "intercept" values
        for peak, values in most_recent_dict["peaks"].items():
            if "slope" not in values:
                print(f"No 'slope' value for peak '{peak}'")
                return False

            slope = values["slope"]
            intercept = values["intercept"]

            lower_limit = -slope * percent_of_slope / 100
            upper_limit = slope * percent_of_slope / 100

            if (intercept <= lower_limit) or (intercept >= upper_limit):
                print(f"intercept value for peak '{peak}' at  '{dilution_factor}'  is outside of acceptable range")
                return False

        # All "intercept" values are in acceptable range
        return True

    def calculate_averages(self, dilution_factors):
        with open(self.output_json, "r") as file:
            json_data = json.load(file)

        averages = {}
        latest_dicts = {}

        # Find the latest (most recent) dicts for each dilution factor
        for factor in dilution_factors:
            latest_dict = None
            latest_date = ""

            for item in json_data:
                if (
                        item["dilution_factor"] == factor
                        and item["compound_name"] == self.solid_name
                ):
                    item_date = item["date"]
                    if item_date > latest_date:
                        latest_date = item_date
                        latest_dict = item

            if latest_dict:
                latest_dicts[factor] = latest_dict

        # Calculate the average slope and intercept for each compound
        for factor, latest_dict in latest_dicts.items():
            peaks = latest_dict["peaks"]

            for compound, values in peaks.items():
                slope = values["slope"]
                intercept = values["intercept"]

                if compound not in averages:
                    averages[compound] = {"slope": 0, "intercept": 0, "count": 0}

                averages[compound]["slope"] += slope
                averages[compound]["intercept"] += intercept
                averages[compound]["count"] += 1

        # Calculate the mean values and store them in a new dictionary
        result = {self.solid_name: {}}

        for compound, values in averages.items():
            mean_slope = values["slope"] / values["count"]
            mean_intercept = values["intercept"] / values["count"]
            result[self.solid_name][compound] = {
                "mean_slope": mean_slope,
                "mean_intercept": mean_intercept,
            }

        # Load existing output dicts from the JSON file
        try:
            with open(self.average_output_json, "r") as file:
                output_dicts = json.load(file)
        except FileNotFoundError:
            output_dicts = []

        # Append the new result to the list
        output_dicts.append(result)

        # Save the updated list of output dicts to the JSON file
        with open(self.average_output_json, "w") as file:
            json.dump(output_dicts, file, indent=4)

    def get_slope_and_intercept(self, peak_label):
        with open(self.average_output_json) as file:
            data = json.load(file)

            # Find the solid matching the solid_name
            for item in data:
                if self.solid_name in item:
                    solid_data = item[self.solid_name]

                    # Find the peak matching the peak_label within the solid
                    if peak_label in solid_data:
                        peak_data = solid_data[peak_label]
                        return peak_data['mean_slope'], peak_data['mean_intercept']

        # Return None if the solid_name or peak_label is not found
        return None, None

if __name__ == '__main__':
    from ur.self_driving.configuration.self_driving_config import standard_curve
    from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, \
        quantos_handling, hplc_handling, centrifuge, shaker_handling, hplc_comm


    hplc_file = data_handling.find_most_recent_hplc_data()
    standard_curve.make_standard_curve(hplc_file=hplc_file, number_of_samples=4,
                                           initial_mass=5.1, initial_volume=1.1,
                                           dilution_factor=3,
                                           min_r2=0.99)

    print('done')


