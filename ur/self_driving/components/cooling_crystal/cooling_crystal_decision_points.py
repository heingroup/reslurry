from ur.self_driving.configuration.self_driving_config import cryst
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling,\
quantos_handling,hplc_handling, centrifuge, shaker_handling, hplc_comm

if __name__ == '__main__':



    cryst.run_cooling_crystallization()

    hplc_file = data_handling.find_most_recent_hplc_data()
    # hplc_file = 'C:/Users/User/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data/2023-09-05/2023-09-05-dasabuvir-wash_5_samples_3_duplicates.csv'
    output_file = 'output.csv'
    cryst.average_hplc_duplicates(hplc_file=hplc_file,output_file=output_file,remove_outliers=True)

    # Parse the HPLC data based on number of samples / duplicates
    # Should also factor in dilution factor
    # Link it with experiment parameters (high temp, low temp, cooling rate, solvent, solid loading)
    # Calculate % yield of product of interest and % purity (peak area pdt / sum of peak area)
    ## pandas to json or pandas to csv

