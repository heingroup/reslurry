from ur.components.vial_handling.vial_handling import VialHandling
from ur.components.hplc_comm.hplc_comm import HPLC_Comm
from ur.components.hplc_handling.hplc_handling import HplcHandling
from ur.components.samplomatic_handling.samplomatic_handling import SamplomaticHandling
from ur.components.quantos_handling.quantos_handling import QuantosHandling
from ur.components.shaker_handling.shaker_handling import ShakerHandling
from ur.components.capper.cap_handling import CapHandling

from ur.components.samplomatic.samplomatic import Samplomatic
from ika import Thermoshaker
from ur.components.data_handling.data_handling import DataHandling
import time
from ur.configuration import deck_consumables
import pandas as pd
import json
from datetime import datetime
import os
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np

class Cooling_Crystal:
    def __init__(self, solid_name, hplc_runtime: float, sample_config_csv: str, system_config_csv: str,
                 vial_handling: VialHandling, hplc_handling: HplcHandling, hplc_comm: HPLC_Comm,
                 samplomatic: Samplomatic, somatic: SamplomaticHandling, ts: Thermoshaker,
                 data_handling: DataHandling, quantos_handling: QuantosHandling, shaker_handling: ShakerHandling,
                 cap_handling = CapHandling):
        self.solid_name = solid_name
        self.hplc_runtime = hplc_runtime
        self.sample_config_csv = sample_config_csv
        self.system_config_csv = system_config_csv
        self.vial_handling = vial_handling
        self.hplc_comm = hplc_comm
        self.hplc_handling = hplc_handling
        self.samplomatic = samplomatic
        self.somatic = somatic
        self.ts = ts
        self.data_handling = data_handling
        self.quantos_handling = quantos_handling
        self.shaker_handling = shaker_handling
        self.cap_handling = cap_handling
        self.shake_speed = 600

    def weigh_sample(self, shaker_index):
        self.vial_handling.vial_from_shaker(shaker_index=shaker_index)
        self.vial_handling.home()
        self.quantos_handling.vial_to_holder(vial_type='hplc_in')
        vial_solvent_mass = (self.quantos_handling.weigh_with_quantos()) * 1000
        self.quantos_handling.vial_from_holder(vial_type='hplc_out')
        self.vial_handling.home()
        self.vial_handling.cup_to_shaker(shaker_index=shaker_index)
        return vial_solvent_mass

    def weigh_diluent_vial(self, diluent_vial_index):
        self.vial_handling.vial_from_tray(vial_index=diluent_vial_index)
        self.vial_handling.home()
        self.quantos_handling.vial_to_holder(vial_type='hplc_in')
        vial_solvent_mass = (self.quantos_handling.weigh_with_quantos()) * 1000
        self.quantos_handling.vial_from_holder(vial_type='hplc_out')
        self.vial_handling.home()
        self.vial_handling.vial_to_tray(vial_index=diluent_vial_index)
        return vial_solvent_mass

    def run_cooling_crystallization(self, clear_shaker: bool = True):
        parameter_config = pd.read_csv(self.system_config_csv, index_col=False)
        number_of_checks = 1
        temp_increment = 5
        equilibration_time = 1
        self.ts.set_speed = 600

        config_file_path = self.sample_config_csv
        config = pd.read_csv(config_file_path, index_col=False)
        df_parameter = parameter_config.iloc[0]
        self.data_handling.set_workflow('cooling_cryst')
        number_of_temps = df_parameter["number_of_temps"]
        temps = []
        time_temps =[]
        pre_sample_masses = {}
        post_sample_masses = {}
        diluent_solvent_masses = {}
        hplc_vials = {}
        run_numbers = {}
        for n in range(number_of_temps):
            current_temp = 'temp_' + str(n+1)
            current_time_temp = 'time_temp_' + str(n+1)
            temps.append(int(df_parameter[current_temp]))
            time_temps.append(int(df_parameter[current_time_temp]))

        number_of_samples = config.shape[0]

        cooling_rate_deg_per_min = float(df_parameter["cooling_rate_deg_per_min"]) # Cooling rate (total time spent cooling)
        rest_time = int(df_parameter["rest_time_min"]) # Time between end of shaking and drawing sample
        duplicates = int(df_parameter["duplicates"]) # Number of duplicate HPLC samples (DO NOT have weigh_diluent_solvent set to True)
        shaker_index = ['A1','B1','C1','D1','A2','B2','C2','D2','A3','B3','C3','D3']
        weigh_solvent = bool(df_parameter['weigh_solvent']) # Weigh after shaking to check for solvent loss
        print(f'weigh_solvent is {str(weigh_solvent)}')
        weigh_diluent_solvent = bool(df_parameter["weigh_diluent_solvent"]) # Weighs HPLC vials after dilution
        check_dissolution = True
        number_of_injections = number_of_samples * duplicates
        self.hplc_comm.send_tcp_message(f'{number_of_temps}_temp_{number_of_injections}_injections')

        ########

        for i in range(number_of_samples):
            df = config.iloc[i]
            vial_index = df["vial_index"]
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            solvent_ratio = float(df["solvent_ratio"])
            solid_mass = df['mass_mg']
            slurry_volume = df["slurry_volume"]
            waste_volume = df["waste_volume"]

            stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
            stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]

            if vial_index == 'None':
                vial_index = deck_consumables.get_clean_vial()[0]
                df['vial_index'] = vial_index

                self.vial_handling.vial_from_tray(vial_index=vial_index, grab_cap=True)
                self.vial_handling.home()
                self.quantos_handling.vial_to_holder(vial_type='hplc')
                vial_mass = (self.quantos_handling.weigh_with_quantos()) * 1000
                self.quantos_handling.vial_from_holder(vial_type='hplc')
                df['vial_mass'] = vial_mass
                # uncap
                self.cap_handling.uncap()
                self.cap_handling.vial_from_capper()
                self.cap_handling.vial_to_holder()
                self.quantos_handling.switch_to_horizontal()
                dosed_mass = self.quantos_handling.dose_with_quantos(solid_weight=float(solid_mass),
                                                                vial_type='hplc')
                df['mass_mg'] = dosed_mass
                self.quantos_handling.switch_to_vertical()

                self.cap_handling.vial_from_holder()
                self.cap_handling.vial_to_capper()
                self.cap_handling.cap()

                # Replace with SAFE location similar to centrifuge.home()
                self.vial_handling.home()


            slurry_volume_A = slurry_volume * solvent_ratio
            slurry_volume_B = slurry_volume * (1 - solvent_ratio)

            if i == 0:
                self.samplomatic.prime(2)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=waste_volume, stock_index=stock_index_A)
            else:
                self.samplomatic.prime(1)
                self.somatic.grab_samplomatic()
                self.somatic.aspirate_stock_solvent(volume_ml=waste_volume, stock_index=stock_index_A)

            self.somatic.aspirate_stock_solvent(volume_ml = slurry_volume_A, stock_index = stock_index_A)
            if stock_index_B != None:
                self.somatic.aspirate_stock_solvent(volume_ml=slurry_volume_B, stock_index=stock_index_B, airgap=False)

            self.somatic.dispense_hplc_grid(volume_ml=slurry_volume, vial_index=vial_index, backend=False)
            self.somatic.return_samplomatic()

            self.vial_handling.vial_from_tray(vial_index=vial_index)
            self.vial_handling.cup_to_shaker(shaker_index = shaker_index[i])
            prev_solvent_A = solvent_A

            config.iloc[i] = df

        config.to_csv(config_file_path, index=False)
        config = pd.read_csv(config_file_path)
        self.samplomatic.prime(1)

        ######

        if weigh_solvent:
            self.vial_handling.home()
            self.quantos_handling.switch_to_horizontal()
            mass_quantos_holder = (self.quantos_handling.weigh_with_quantos()) * 1000
            self.quantos_handling.switch_to_vertical()
            self.vial_handling.home()

        for t in range(number_of_temps):
            temp = temps[t]
            time_temp = time_temps[t]
            pre_sample_masses = {}
            post_sample_masses = {}
            diluent_solvent_masses = {}
            diluent_vial_index_dict = {}

            if t == 0:
                # ADD CAMERA FUNCTION
                self.ts.start_shaking()
                self.ts.set_temperature = temp
                time.sleep(1)
                self.ts.start_tempering()
                time.sleep(time_temp*60)
                self.ts.stop_shaking()

                if check_dissolution:
                    for s in range(number_of_samples):
                        df = config.iloc[s]
                        vial_index = df["vial_index"]
                        self.vial_handling.take_picture_of_vial(vial_index=vial_index, shaker_index=shaker_index[s], temperature=temp)

            else:
                self.ts.start_shaking()
                temper_time = int((temps[t-1] - temp) / cooling_rate_deg_per_min)
                self.shaker_handling.temper_over_time_with_hplc(start_temp=temps[t-1], end_temp=temp, time_min=temper_time,
                                                           hplc_runtime=self.hplc_runtime)
                self.shaker_handling.shake_with_hplc(shake_time=time_temp, hplc_runtime=self.hplc_runtime)
                self.ts.stop_shaking()

            self.hplc_handling.run_blank()

            for i in range(number_of_samples):
                df = config.iloc[i]
                vial_index = df["vial_index"]
                slurry_volume = float(df["slurry_volume"])
                sample_volume = float(df["sample_volume"])
                dilution_factor = float(df["dilution_factor"])
                mass_mg = float(df['mass_mg'])
                vial_mass = float(df["vial_mass"])
                solvent_A = df["solvent_A"]
                solvent_B = df["solvent_B"]
                solvent_ratio = float(df["solvent_ratio"])

                diluent_volume = sample_volume * dilution_factor

                if weigh_solvent:
                    vial_solvent_mass = self.weigh_sample(shaker_index=shaker_index[i])
                    pre_sample_solvent_mass = vial_solvent_mass - mass_mg - vial_mass - mass_quantos_holder
                    print(f'{vial_index} pre sample solvent mass at {temp} is {pre_sample_solvent_mass}')


                self.hplc_handling.run_next_vial_in_queue_if_ready(hplc_runtime=self.hplc_runtime)
                self.vial_handling.push_shaker()
                time.sleep(rest_time * 60)

                diluent_vial_index_list = []
                diluent_solvent_mass_list = []
                for d in range(duplicates):
                    if i == 0:
                        self.samplomatic.prime(volume_ml=2)
                    else:
                        self.samplomatic.prime(0.2)

                    self.somatic.grab_samplomatic()
                    if diluent_volume <= 0.3:
                        diluent_vial_index = deck_consumables.get_clean_vial(insert=True)[0]
                    else:
                        diluent_vial_index = deck_consumables.get_clean_vial()[0]
                    diluent_vial_index_list.append(diluent_vial_index)
                    self.somatic.draw_sample_from_shaker(volume_ml=sample_volume, shaker_index=shaker_index[i], type='hplc')
                    self.somatic.dispense_hplc_grid(volume_ml=diluent_volume, vial_index=diluent_vial_index)
                    self.somatic.return_samplomatic()

                    if weigh_diluent_solvent:
                        diluent_vial_mass = deck_consumables.get_vial_mass(diluent_vial_index)
                        vial_solvent_mass = self.weigh_diluent_vial(diluent_vial_index=diluent_vial_index)
                        diluent_solvent_mass = vial_solvent_mass - diluent_vial_mass - mass_quantos_holder
                        diluent_solvent_mass_list.append(diluent_solvent_mass)

                    self.hplc_handling.add_to_queue_and_run_if_ready(vial_index=diluent_vial_index, hplc_runtime=self.hplc_runtime)
                    diluent_vials = ','.join(diluent_vial_index_list)
                    diluent_solvent_mass = ','.join(map(str, diluent_solvent_mass_list))

                if weigh_solvent:
                    vial_solvent_mass = self.weigh_sample(shaker_index=shaker_index[i])
                    post_sample_solvent_mass = vial_solvent_mass - mass_mg - vial_mass - mass_quantos_holder
                    print(f'{vial_index} post sample solvent mass at {temp} is {post_sample_solvent_mass}')

                self.data_handling.new_sample()
                save_dict = locals()
                save_list = ['vial_index', 'diluent_vials', 'mass_mg', 'slurry_volume', 'solvent_A', 'solvent_B',
                             'solvent_ratio', 'solid_name', 'rest_time','sample_volume', 'dilution_factor', 'temp',
                             'time_temp', 'cooling_rate_deg_per_min']

                if weigh_solvent:
                    save_list = save_list + ['pre_sample_solvent_mass', 'post_sample_solvent_mass']
                if weigh_diluent_solvent:
                    save_list = save_list + ['diluent_solvent_mass']
                for s in save_list:
                    self.data_handling.save_value(data_name=s, data_value=str(save_dict.get(s)))

        self.hplc_handling.run_remainder_in_queue(hplc_runtime=self.hplc_runtime)

        self.vial_handling.above_hplc_tray_front()
        self.hplc_handling.return_last_vial()
        time.sleep(self.hplc_runtime*60)
        self.hplc_handling.process_data_and_send_csv()
        time.sleep(10)

        if clear_shaker:
            for x in range(number_of_samples):
                df = config.iloc[x]
                vial_index = df["vial_index"]
                self.vial_handling.vial_from_shaker(shaker_index=shaker_index[x])
                self.vial_handling.vial_to_tray(vial_index=vial_index)

    def clear_shaker(self):
        shaker_index = ['A1','B1','C1','D1','A2','B2','C2','D2','A3','B3','C3','D3']

        config_file_path = self.sample_config_csv
        config = pd.read_csv(config_file_path, index_col=False)

        number_of_samples = config.shape[0]

        for x in range(number_of_samples):
            df = config.iloc[x]
            vial_index = df["vial_index"]
            self.vial_handling.vial_from_shaker(shaker_index=shaker_index[x])
            self.vial_handling.vial_to_tray(vial_index=vial_index)


    def average_hplc_duplicates(self, hplc_file, output_file, remove_outliers: bool = False):
        df = pd.read_csv(hplc_file, index_col=False)

        parameter_config = pd.read_csv(self.system_config_csv, index_col=False)
        df_parameter = parameter_config.iloc[0]
        duplicates = int(df_parameter["duplicates"]) # Number of duplicate HPLC samples

        averaged_values = []

        # Process the data in chunks of size duplicates
        for i in range(0, len(df), duplicates):
            chunk = df.iloc[i:i + duplicates]

            # Exclude the first column (string values)
            chunk = chunk.iloc[:, 1:]

            # Check if columns contain only numeric data
            numeric_columns = chunk.select_dtypes(include=[np.number])

            if not numeric_columns.empty:
                # Calculate the average for each numeric column
                avg_values = numeric_columns.mean()

                if remove_outliers:
                    # Calculate the relative standard deviation
                    stdev_values = numeric_columns.std()

                    # Calculate the relative standard deviation as a percentage
                    relative_stdev = (stdev_values / avg_values) * 100

                    # Check if any relative standard deviation is above 10%
                    if (relative_stdev > 10).any():
                        # Calculate the sum of absolute differences for each column
                        sum_diff = numeric_columns.sub(numeric_columns.mean()).abs().sum(axis=0)

                        # Find the column with the highest sum of differences
                        column_to_remove = sum_diff.idxmax()

                        # Remove the furthest value from the average calculation
                        chunk = chunk.drop(columns=[column_to_remove])

                        # Recalculate the average after removing the furthest value
                        avg_values = chunk.mean()

                # Append the averaged values to the list
                averaged_values.append(avg_values)

        if averaged_values:
            # Create a new DataFrame with the averaged values
            result_df = pd.concat(averaged_values, axis=1).T

            # Save the new DataFrame to a CSV file
            result_df.to_csv(output_file, index=False)

        # Create a new DataFrame with the averaged values
        result_df = pd.concat(averaged_values, axis=1).T

        # Save the new DataFrame to a CSV file
        result_df.to_csv(output_file, index=False)









