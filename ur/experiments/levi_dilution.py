import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling
import time
import logging
from datetime import datetime

number_of_samples = 1
slope = 1.3076
intercept = -0.0056
#change to count
levi_config = pd.read_csv("levi_config.csv")

for n in range(number_of_samples):
    df = levi_config.iloc[n]
    shaker_index = ['A1', 'B1', 'C1', 'D1', 'A2', 'B2', 'C2', 'D2']
    cup_index = ['A1', 'A2', 'A3', 'A4', 'B1', 'B2', 'B3', 'B4']
    sample_volume = float(df["sample_volume_ml"])
    sample_volume_calibrated = (sample_volume * slope) + intercept
    dilution_factor = float(df["dilution_factor"])
    hplc_vial_index = deck_consumables.get_clean_vial(with_stirbar=False)[0]

    if n == 0:
        samplomatic.prime(volume_ml=1)

    vial_handling.push_filter_in_shaker(shaker_index=shaker_index[n])
    vial_handling.cup_from_shaker(shaker_index=shaker_index[n])
    vial_handling.cup_to_tray(cup_index=cup_index[n])

    samplomatic.prime(volume_ml=0.2)
    somatic.grab_samplomatic()
    somatic.draw_sample(volume_ml=sample_volume_calibrated, sample_index=cup_index[n])
    somatic.dispense_hplc_grid(volume_ml=sample_volume_calibrated * dilution_factor, vial_index=hplc_vial_index)
    somatic.return_samplomatic()