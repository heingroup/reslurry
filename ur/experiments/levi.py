import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling, quantos_handling
import time
import logging
from datetime import datetime

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

'''
todo
-add a check for clean vials/cups...if there are not enough the reaction will not run
'''

def record_timestamp(process: str = "levi", batch: str = 1, sample: str = 1, system: str = 'UR', action: str = 'start'):
    msg = process + ", " + system + ", " + action
    print(msg)
    with open(r'C:\Users\User\PycharmProjects\automated_solubility_h1\ur\experiments\levi_metadata.csv','a+') as f:
        f.write(f'{msg},{datetime.now()}\n')

if __name__ == '__main__':
    #SETTINGS - CHANGE BEFORE YOU BEGIN RUN
    number_of_duplicates = 1

    #CHECKLIST - DID YOU...?
    #CHANGE LEVI_CONFIG
    #REFILL SOLVENTS
    #REFILL CUPS
    #REFILL FILTERS
    #MAKE SURE ALIGNER AND SHAKER ARE EMPTY

    i = 0
    levi_config = pd.read_csv("levi_config.csv")
    levi_config_sorted = levi_config.sort_values('temperature').reset_index()
    number_of_temperatures = levi_config_sorted["temperature"].nunique()

    for t in range(number_of_temperatures):
        # Groups data into temperatures batches and count how many are in each. Each t is one temperature batch
        temperature_count = levi_config_sorted.groupby(['temperature']).size().reset_index(name='count')
        number_of_samples = temperature_count.loc[t, "count"]

        # Set temperature and shaker speed for current batch, begin heating/cooling
        df = levi_config_sorted.iloc[i]
        ts.set_temperature = int(df["temperature"])
        ts.set_speed = 350
        # time.sleep(1)
        ts.start_tempering()

        samplomatic.prime(volume_ml=1.5)

        vial_handling.home()

        vial_handling.open_shaker_gripper()

        for n in range(number_of_samples):
            data_handling.append_new_experiment()
            df = levi_config_sorted.iloc[i]

            solid_name = df["solid_name"]
            solid_mass = df["solid_mass_g"]
            predosed_cup_index = df["predosed_cup_index"]
            predosed_filter_index = df["predosed_filter_index"]
            temperature = int(df["temperature"])
            duration = int(df["duration_min"])
            shaker_rpm = int(df["shaker_rpm"])
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            solvent_ratio = float(df["solvent_ratio"])
            slurry_volume = float(df["slurry_volume_ml"])
            sample_volume = float(df["sample_volume_ml"])
            dilution_factor = int(df["dilution_factor"])

            if predosed_cup_index == "None":
                cup_index = deck_consumables.get_clean_cup()

                vial_handling.cup_from_tray(cup_index=cup_index)
                vial_handling.home()
                quantos_handling.vial_to_holder(vial_type='push_filter')

                dosed_mass = quantos_handling.dose_with_quantos(solid_weight=float(solid_mass*1000), vial_type='push_filter')
                solid_mass = dosed_mass/1000

                quantos_handling.vial_from_holder(vial_type='push_filter')
                vial_handling.cup_to_tray(cup_index=cup_index)
            else:
                cup_index = predosed_cup_index

            if predosed_filter_index == "None":
                filter_index = deck_consumables.get_clean_filter()
            else:
                filter_index = predosed_filter_index

            save_dict = locals()
            save_list = ['solid_name', 'solid_mass', 'cup_index', 'filter_index', 'temperature', 'duration',
                         'shaker_rpm', 'solvent_A', 'solvent_B', 'solvent_ratio', 'slurry_volume',
                         'sample_volume', 'dilution_factor']
            for s in save_list:
                data_handling.save_value(data_name = s, data_value = save_dict.get(s))

            stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
            stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]

            r_squared, slope, intercept = deck_consumables.get_calibration_info(solvent_A)
            slurry_volume_calibrated_A = (slurry_volume -intercept) /slope
            slurry_volume_calibrated_B = 0

            somatic.grab_samplomatic()
            somatic.aspirate_stock_solvent(volume_ml = slurry_volume_calibrated_A * solvent_ratio, stock_index = stock_index_A)

            if stock_index_B != None:
                r_squared, slope, intercept = deck_consumables.get_calibration_info(solvent_B)
                slurry_volume_calibrated_B = (slurry_volume - intercept) /slope
                somatic.aspirate_stock_solvent(volume_ml=slurry_volume_calibrated_B * (1 - solvent_ratio),
                                               stock_index=stock_index_B)

            somatic.dispense_solvent_in_cup(volume_ml=slurry_volume_calibrated_A + slurry_volume_calibrated_B,
                                            sample_index=cup_index)
            somatic.return_samplomatic()

            vial_handling.cup_from_tray(cup_index = cup_index)
            vial_handling.cup_to_aligner()
            vial_handling.filter_from_tray(filter_index = filter_index, large_filter = True)
            vial_handling.align_filter()
            vial_handling.insert_filter()
            vial_handling.cup_from_aligner()

            shaker_index = deck_consumables.get_empty_shaker_location('sample')
            data_handling.save_value(data_name="shaker_index", data_value = shaker_index)

            vial_handling.cup_to_shaker(shaker_index=shaker_index)

            i += 1

        for n in range(number_of_samples):
            df = levi_config_sorted.iloc[n]
            save_index = data_handling.count_experiments() - number_of_samples + n
            shaker_index = data_handling.read_value(i = save_index, data_name = "shaker_index")
            cup_index = data_handling.read_value(i = save_index, data_name = "cup_index")
            sample_volume = data_handling.read_value(i = save_index, data_name = "sample_volume")
            r_squared, slope, intercept = deck_consumables.get_calibration_info('THF')
            sample_volume_calibrated = (sample_volume-intercept) / slope
            dilution_factor = data_handling.read_value(i=save_index, data_name="dilution_factor")
            hplc_vial_index = deck_consumables.get_clean_vial(with_stirbar=False)[0]
            data_handling.save_value(data_name = "hplc_vial_index", data_value = hplc_vial_index)

            if n == 0:
                duration = int(df["duration_min"])
            else:
                duration = int(df["duration_min"]) - int(levi_config_sorted.iloc[n-1]["duration_min"])
            time.sleep(1)
            ts.set_speed = int(df["shaker_rpm"])
            ts.start_shaking()
            time.sleep((duration * 60) + 1)
            if n == 0:
                samplomatic.prime(volume_ml=0.5)
            ts.stop_shaking()

            vial_handling.push_filter_in_shaker(shaker_index=shaker_index)
            vial_handling.cup_from_shaker(shaker_index=shaker_index)
            vial_handling.cup_to_tray(cup_index=cup_index)

            samplomatic.prime(volume_ml=0.2)
            somatic.grab_samplomatic()
            somatic.draw_sample_from_cup(volume_ml=sample_volume_calibrated, sample_index=cup_index)
            somatic.dispense_hplc_grid(volume_ml = sample_volume_calibrated * dilution_factor, vial_index=hplc_vial_index)
            somatic.return_samplomatic()


















