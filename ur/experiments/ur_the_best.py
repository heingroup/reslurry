from hein_robots.robotics import Location, Cartesian
from hein_robots.grids import Grid, StaticGrid
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence

UR_arm = UR3Arm('192.168.254.88')
ur = UR_arm

def main():
    message = 'kill all humans'
    letters = list(message)

    line_1_list = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']']
    line_2_list = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';']
    line_3_list = ['z', 'x', 'c', 'v', 'b', 'n', 'm']

    line_1_dict = {'q':'A1', 'w':'A2', 'e':'A3', 'r':'A4', 't':'A5', 'y':'A6', 'u':'A7', 'i':'A8', 'o':'A9', 'p':'A10', '[':'A11', ']':'A12'}
    line_2_dict = {'a':'A1', 's':'A2', 'd':'A3', 'f':'A4', 'g':'A5', 'h':'A6', 'j':'A7', 'k':'A8', 'l':'A9', ';':'A10', "'":'A11', 'enter':'A12'}
    line_3_dict = {'z':'A1', 'x':'A2', 'c':'A3', 'v':'A4', 'b':'A5', 'n':'A6', 'm':'A7'}

    grid_line_1 = Grid(Location(x=211.8, y=259.4, z=97.2, rx=0, ry=0, rz=0), rows=12, columns=1,
                       spacing=Cartesian(18.9, 18.9, 0), offset=Location(rx=-180, ry=0, rz=90))
    grid_line_2 = Grid(Location(x=230.8, y=263.6, z=97.2, rx=0, ry=0, rz=0), rows=12, columns=1,
                       spacing=Cartesian(18.9, 18.9, 0), offset=Location(rx=-180, ry=0, rz=90))
    grid_line_3 = Grid(Location(x=249.3, y=272.7, z=97.2, rx=0, ry=0, rz=0), rows=7, columns=1,
                       spacing=Cartesian(18.9, 18.9, 0), offset=Location(rx=-180, ry=0, rz=90))
    space = Grid(Location(x=271.2, y=356, z=97.7, rx=0, ry=0, rz=0), rows=2, columns=1,
                       spacing=Cartesian(18.9, 18.9, 0), offset=Location(rx=-180, ry=0, rz=90))
    filter_grid_large = Grid(Location(x=143.9, y=103.9, z=90, rx=0, ry=0, rz=0), rows=3, columns=4, spacing=Cartesian(20.2, 20.2, 0),
                    offset=Location(rx=-180, ry=0, rz=90))

    ur.open_gripper(0.65)
    ur.move_to_location(filter_grid_large.locations['A1'] + Location(z=60), velocity=120)
    ur.move_to_location(filter_grid_large.locations['A1'] + Location(z=5), velocity=120)
    ur.open_gripper(0.95)
    ur.move_to_location(filter_grid_large.locations['A1'] + Location(z=60), velocity=120)
    vel = 500
    for l in letters:
        if l in line_1_list:
            index = line_1_dict.get(l)
            ur.move_to_location(grid_line_1.locations[index] + Location(z=5), velocity=vel)
            ur.move_to_location(grid_line_1.locations[index], velocity=vel)
            ur.move_to_location(grid_line_1.locations[index] + Location(z=5), velocity=vel)

        if l in line_2_list:
            index = line_2_dict.get(l)
            ur.move_to_location(grid_line_2.locations[index] + Location(z=5), velocity=vel)
            ur.move_to_location(grid_line_2.locations[index], velocity=vel)
            ur.move_to_location(grid_line_2.locations[index] + Location(z=5), velocity=vel)

        if l in line_3_list:
            index = line_3_dict.get(l)
            ur.move_to_location(grid_line_3.locations[index] + Location(z=5), velocity=vel)
            ur.move_to_location(grid_line_3.locations[index], velocity=vel)
            ur.move_to_location(grid_line_3.locations[index] + Location(z=5), velocity=vel)

        if l == ' ':
            index = 'A1'
            ur.move_to_location(space.locations[index] + Location(z=5))
            ur.move_to_location(space.locations[index])
            ur.move_to_location(space.locations[index] + Location(z=5))

    # ur.move_to_location(grid_line_2.locations['A12'] + Location(z=5))
    # ur.move_to_location(grid_line_2.locations['A12'])
    # ur.move_to_location(grid_line_2.locations['A12'] + Location(z=5))

    ur.move_to_location(filter_grid_large.locations['A1'] + Location(z=60))
    ur.move_to_location(filter_grid_large.locations['A1'] + Location(z=5))
    ur.open_gripper(0.65)
    ur.move_to_location(filter_grid_large.locations['A1'] + Location(z=60))

if __name__ == '__main__':
    main()