import pandas as pd

from ur.configuration.ur_deck import centrifuge, quantos_handling, somatic, stir_handling

VIAL_TYPE = 'centrifuge'
STIR_DURATION = 30  # in s
CENTRIFUGE_DURATION = 30  # in s
DISPENSE_VOLUME = 1  # ml
CURRENT_VIAL = 0
TRAY = ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'A2', 'B2', 'C2', 'D2', 'E2', 'F2']\
# TRAY = ['A2', 'B2', 'C2', 'D2', 'E2', 'F2']

MAX_DISPENSE = 3
SOLID_WEIGHT = 5 #mg


def run(current_vial, solid_weight=SOLID_WEIGHT, dispense_volume=DISPENSE_VOLUME,
        stir_duration=STIR_DURATION, centrifuge_duration=CENTRIFUGE_DURATION):
    if not current_vial + 1 < len(TRAY):  # check at least 2 available tubes
        print(current_vial, "no more vials")
        exit()
    centrifuge.tube_from_tray(TRAY[current_vial])
    # open cap and dose with quantos
    centrifuge.tube_to_tray('A3')
    centrifuge.open_tube('front')
    centrifuge.tube_from_tray('A3', filter=True)
    quantos_handling.vial_to_holder(vial_type=VIAL_TYPE)
    # quantos_handling.switch_to_horizontal()
    # quantos_handling.dose_with_quantos(solid_weight=solid_weight, vial_type=VIAL_TYPE)
    # quantos_handling.switch_to_vertical()
    quantos_handling.vial_from_holder(vial_type=VIAL_TYPE)
    centrifuge.tube_to_tray('A3', filter=True)

    dissolved = False
    dispense_count = 0
    while not dissolved and dispense_count < MAX_DISPENSE:  # todo
        print(TRAY[current_vial])
        # centrifuge.tube_to_tray('A3')
        # somatic.aspirate_stock_solvent(volume_ml=dispense_volume)
        # somatic.dispense_solvent_in_tube() todo
        dispense_count += 1

        centrifuge.close_tube('front')
        centrifuge.tube_from_tray('A3')
        dissolved = stir_handling.run_stir(stir_duration=stir_duration)

        centrifuge.tube_to_tray('A3')
        centrifuge.run_centrifuge(centrifuge_duration=centrifuge_duration)
        if dissolved:
            centrifuge.tube_from_tray('A3')
            centrifuge.tube_to_tray(TRAY[current_vial])
        else:
            centrifuge.tube_from_tray(TRAY[current_vial + 1])
            centrifuge.tube_from_tray('C3')
            centrifuge.close_tube('front')
            centrifuge.close_tube('back')
            centrifuge.switch_filter()
            centrifuge.close_tube('front')
            # centrifuge.close_tube('back')
            centrifuge.tube_from_tray('A3')
            centrifuge.tube_to_tray(TRAY[current_vial])
            centrifuge.tube_from_tray('C3')
            centrifuge.tube_to_tray('A3')
            current_vial += 1
    centrifuge.tube_to_tray(TRAY[current_vial])

    current_vial += 1  # start a new solubility test
    return current_vial


def parse_data(sample_data):
    solid_weight = float(sample_data['mass'])
    centrifuge_duration = int(sample_data['cent_time'])
    stir_duration = int(sample_data['stir_time'])
    dispense_volume = float(sample_data['volume'])
    return solid_weight, centrifuge_duration, stir_duration, dispense_volume


if __name__ == '__main__':
    # to do parse date from csv for each run
    # samples = pd.read_csv('solubility_test.csv')
    # for i in range(len(samples)):
    for i in TRAY:
        CURRENT_VIAL = run(current_vial=CURRENT_VIAL, solid_weight=SOLID_WEIGHT, dispense_volume=DISPENSE_VOLUME,
                           centrifuge_duration=CENTRIFUGE_DURATION, stir_duration=STIR_DURATION)

        # solid_weight, centrifuge_duration, stir_duration, dispense_volume = parse_data(samples.iloc[i])
        # CURRENT_VIAL = run(current_vial=CURRENT_VIAL, solid_weight=solid_weight, dispense_volume=dispense_volume,
        #                    centrifuge_duration=centrifuge_duration, stir_duration=stir_duration)
