import pandas as pd
from ur.configuration import deck_consumables
from ur.configuration.ur_deck import filter_handling, vial_handling, somatic, samplomatic, ts, arm, data_handling
import time
import logging
from datetime import datetime

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

'''
todo
-change record timestamp fxn to reflect actual workflow, talk to heinseltzer about it once workflow is done
-change all wrist 3 sequence files to be 0R
-add filtration type to csv and change code
-add get_airgap before getting stock solvent, then dispense half of airgap
-update consumables_config to reflect actual trays, also change consumables functions to reflect actual cups/vials stuff
--need small and large filter
-add a check for clean vials/cups...if there are not enough the reaction will not run
-print filter aligner
-rename self.port_to_prime_from in samplomatic
-hplc dispense needs to change
-set port for sample drawing
'''

def record_timestamp(process: str = "levi", batch: str = 1, sample: str = 1, action: str = 'start'):
    msg = process + ", " + batch + ", " + sample + ", " + action
    print(msg)
    with open(r'C:\Users\User\PycharmProjects\automated_solubility_h1\ur\experiments\levi_metadata.csv','a+') as f:
        f.write(f'{msg},{datetime.now()}\n')

if __name__ == '__main__':
    #SETTINGS - CHANGE BEFORE YOU BEGIN RUN
    use_quantos = False
    number_of_duplicates = 1
    i = 0

    #CHECKLIST - DID YOU...?
    #CHANGE LEVI_CONFIG
    #REFILL SOLVENTS
    #REFILL CUPS
    #REFILL FILTERS
    #MAKE SURE ALIGNER AND SHAKER ARE EMPTY

    levi_config = pd.read_csv("levi_config.csv")
    levi_config_sorted = levi_config.sort_values('temperature').reset_index()
    number_of_temperatures = levi_config_sorted["temperature"].nunique()

    record_timestamp(process='begin_new_experiment', sample='None',
                     batch='total_number_of_batches: ' + str(number_of_temperatures), action='start')

    for t in range(number_of_temperatures):
        # Groups data into temperatures batches and count how many are in each. Each t is one temperature batch
        temperature_count = levi_config_sorted.groupby(['temperature']).size().reset_index(name='count')
        number_of_samples = temperature_count.loc[t, "count"]

        # Set temperature and shaker speed for current batch, begin heating/cooling
        df = levi_config_sorted.iloc[i]
        ts.set_temperature = int(df["temperature"])
        ts.set_speed = 350
        # time.sleep(1)
        ts.start_tempering()

        samplomatic.prime(volume_ml=1.5)

        record_timestamp(process='begin_new_batch_shaker_duration: ' + str(df["shaker_rpm"]), batch=str(t+1),
                         sample='total_number_of_samples: ' + str(number_of_samples), action='start')

        record_timestamp(process='open_shaker_gripper',batch=str(t + 1),sample='None', action='start')
        vial_handling.open_shaker_gripper()
        record_timestamp(process='open_shaker_gripper', batch=str(t + 1), sample='None', action='end')

        for n in range(number_of_samples):
            record_timestamp(process='start_of_sample_preparation', batch=str(t + 1), sample=str(n+1), action='start')
            data_handling.append_new_experiment()
            df = levi_config_sorted.iloc[i]

            solid_name = df["solid_name"]
            solid_mass = df["solid_mass_g"]
            predosed_cup_index = df["predosed_cup_index"]
            predosed_filter_index = df["predosed_filter_index"]
            temperature = int(df["temperature"])
            duration = int(df["duration_min"])
            shaker_rpm = int(df["shaker_rpm"])
            solvent_A = df["solvent_A"]
            solvent_B = df["solvent_B"]
            solvent_ratio = float(df["solvent_ratio"])
            slurry_volume = float(df["slurry_volume_ml"])
            sample_volume = float(df["sample_volume_ml"])
            dilution_factor = int(df["dilution_factor"])

            if predosed_cup_index == "None":
                cup_index = deck_consumables.get_clean_vial()[0]
            else:
                cup_index = predosed_cup_index

            if predosed_filter_index == "None":
                filter_index = deck_consumables.get_clean_filter()
            else:
                filter_index = predosed_filter_index

            save_dict = locals()
            save_list = ['solid_name', 'solid_mass', 'cup_index', 'filter_index', 'temperature', 'duration',
                         'shaker_rpm', 'solvent_A', 'solvent_B', 'solvent_ratio', 'slurry_volume',
                         'sample_volume', 'dilution_factor']
            for s in save_list:
                data_handling.save_value(data_name = s, data_value = save_dict.get(s))

            stock_index_A = deck_consumables.get_stock_info(solvent_A)[2]
            stock_index_B = deck_consumables.get_stock_info(solvent_B)[2]

            record_timestamp(process='grap_samplomatic', batch=str(t + 1), sample=str(n + 1), action='start')
            somatic.grab_samplomatic()
            record_timestamp(process='grap_samplomatic', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='aspirate_stock_solvent', batch=str(t + 1), sample=str(n + 1), action='start')
            somatic.aspirate_stock_solvent(volume_ml = slurry_volume * solvent_ratio, stock_index = stock_index_A)
            record_timestamp(process='aspirate_stock_solvent', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='dispense_solvent_in_cup', batch=str(t + 1), sample=str(n + 1), action='start')
            somatic.dispense_solvent_in_cup(volume_ml=slurry_volume* solvent_ratio, sample_index=cup_index)
            record_timestamp(process='dispense_solvent_in_cup', batch=str(t + 1), sample=str(n + 1), action='end')

            if stock_index_B != None:
                record_timestamp(process='aspirate_stock_solvent', batch=str(t + 1), sample=str(n + 1), action='start')
                somatic.aspirate_stock_solvent(volume_ml=slurry_volume * (1 - solvent_ratio), stock_index=stock_index_B)
                record_timestamp(process='aspirate_stock_solvent', batch=str(t + 1), sample=str(n + 1), action='end')

                record_timestamp(process='dispense_solvent_in_cup', batch=str(t + 1), sample=str(n + 1), action='start')
                somatic.dispense_solvent_in_cup(volume_ml=slurry_volume * (1 - solvent_ratio), sample_index=cup_index)
                record_timestamp(process='dispense_solvent_in_cup', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='return_samplomatic', batch=str(t + 1), sample=str(n + 1), action='start')
            somatic.return_samplomatic()
            record_timestamp(process='return_samplomatic', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='cup_from_tray', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.cup_from_tray(cup_index = cup_index)
            record_timestamp(process='cup_from_tray', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='cup_to_aligner', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.cup_to_aligner()
            record_timestamp(process='cup_to_aligner', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='filter_from_tray', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.filter_from_tray(filter_index = filter_index, large_filter = True)
            record_timestamp(process='filter_from_tray', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='align_filter', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.align_filter()
            record_timestamp(process='align_filter', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='insert_filter', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.insert_filter()
            record_timestamp(process='insert_filter', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='cup_from_aligner', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.cup_from_aligner()
            record_timestamp(process='cup_from_aligner', batch=str(t + 1), sample=str(n + 1), action='end')

            shaker_index = deck_consumables.get_empty_shaker_location('sample')
            data_handling.save_value(data_name="shaker_index", data_value = shaker_index)

            record_timestamp(process='cup_to_shaker', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.cup_to_shaker(shaker_index=shaker_index)
            record_timestamp(process='cup_to_shaker', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='end_of_sample_preparation', batch=str(t + 1), sample=str(n + 1), action='end')

            i += 1

        for n in range(number_of_samples):

            df = levi_config_sorted.iloc[n]
            save_index = data_handling.count_experiments() - number_of_samples + n
            shaker_index = data_handling.read_value(i = save_index, data_name = "shaker_index")
            cup_index = data_handling.read_value(i = save_index, data_name = "cup_index")
            sample_volume = data_handling.read_value(i = save_index, data_name = "sample_volume")
            r_squared, slope, intercept = deck_consumables.get_stock_info('THF')
            sample_volume_calibrated = (sample_volume * slope) + intercept
            dilution_factor = data_handling.read_value(i=save_index, data_name="dilution_factor")
            hplc_vial_index = deck_consumables.get_clean_vial(with_stirbar=False)[0]
            data_handling.save_value(data_name = "hplc_vial_index", data_value = hplc_vial_index)

            if n == 0:
                duration = int(df["duration_min"])
            else:
                duration = int(df["duration_min"]) - int(levi_config_sorted.iloc[n-1]["duration_min"])
            time.sleep(1)
            ts.set_speed = int(df["shaker_rpm"])
            ts.start_shaking()

            record_timestamp(process='shaking_for_' + str(duration) + '_sec', batch=str(t + 1), sample='None', action='start')
            time.sleep((duration * 60) + 1)
            record_timestamp(process='shaking_for_' + str(duration) + '_sec', batch=str(t + 1), sample='None', action='end')

            if n == 0:
                samplomatic.prime(volume_ml=1)
            ts.stop_shaking()

            record_timestamp(process='start_of_sample_filtration', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='push_vial_in_shaker', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.push_filter_in_shaker(shaker_index=shaker_index)
            record_timestamp(process='push_vial_in_shaker', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='cup_from_shaker', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.cup_from_shaker(shaker_index=shaker_index)
            record_timestamp(process='cup_from_shaker', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='cup_to_tray', batch=str(t + 1), sample=str(n + 1), action='start')
            vial_handling.cup_to_tray(cup_index=cup_index)
            record_timestamp(process='cup_to_tray', batch=str(t + 1), sample=str(n + 1), action='end')

            samplomatic.prime(volume_ml=0.2)

            record_timestamp(process='grab_samplomatic', batch=str(t + 1), sample=str(n + 1), action='start')
            somatic.grab_samplomatic()
            record_timestamp(process='grab_samplomatic', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='draw_sample_from_cup', batch=str(t + 1), sample=str(n + 1), action='start')
            somatic.draw_sample_from_cup(volume_ml=sample_volume_calibrated, sample_index=cup_index)
            record_timestamp(process='draw_sample_from_cup', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='dispense_hplc_grid', batch=str(t + 1), sample=str(n + 1), action='start')
            somatic.dispense_hplc_grid(volume_ml = sample_volume_calibrated * dilution_factor, vial_index=hplc_vial_index)
            record_timestamp(process='dispense_hplc_grid', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='return_samplomatic', batch=str(t + 1), sample=str(n + 1), action='start')
            somatic.return_samplomatic()
            record_timestamp(process='return_samplomatic', batch=str(t + 1), sample=str(n + 1), action='end')

            record_timestamp(process='end_of_sample_filtration', batch = str(t+1), sample = str(n+1), action='end')

        record_timestamp(process='end_of_batch', batch=str(t+1),
                         sample='total_number_of_samples: ' + str(number_of_samples), action='end')

    record_timestamp(process='end_experiment', sample='None',
                     batch='total_number_of_batches: ' + str(number_of_temperatures), action='end')















