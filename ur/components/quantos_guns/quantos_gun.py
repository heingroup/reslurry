import csv
import datetime
from typing import Optional
import math
import time
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from hein_robots.robotics import Location
from ur.configuration import deck_consumables
from hein_robots.grids import StaticGrid
from mtbalance import ArduinoAugmentedQuantos


class QuantosGuns:

    def __init__(self, ur: UR3Arm, quantos: ArduinoAugmentedQuantos,
                 wall_sequence_file: str, quantos_sequence_file: str, wall_grid: StaticGrid,
                 wall_approach_far_offset: Location = Location(x=75, z=2),
                 wall_approach_close_offset: Location = Location(x=45, z=2),
                 joint_velocity: float = 60, linear_velocity: float = 80):
        self.ur = ur
        self.quantos = quantos
        self.wall_sequence = URScriptSequence(wall_sequence_file)
        self.quantos_sequence = URScriptSequence(quantos_sequence_file)
        self.wall_grid = wall_grid
        # self.wall_grid = StaticGrid(self.wall_sequence[self.WALL_GRID_SEQUENCE_NAMES], rows=3, columns=2)
        self.wall_approach_far_offset = wall_approach_far_offset
        self.wall_approach_close_offset = wall_approach_close_offset
        self.joint_velocity = joint_velocity
        self.linear_velocity = linear_velocity

    def write_dict_to_csv(self, time_stmp, exp_name, command):
        field_names = ['TIME', 'EXP_NAME', 'COMMAND']
        # Dictionary
        dict = {'TIME': time_stmp, 'EXP_NAME': exp_name, 'COMMAND': command}
        # Open your CSV file in append mode
        # Create a file object for this file
        with open('event_2lbs.csv', 'a') as f_object:
            # Pass the file object and a list
            # of column names to DictWriter()
            # You will get a object of DictWriter
            dictwriter_object = csv.DictWriter(f_object, fieldnames=field_names)

            # Pass the dictionary as an argument to the Writerow()
            dictwriter_object.writerow(dict)

            # Close the file object
            f_object.close()

    def place_gun_on_wall(self, wall_index: str):
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_close_offset,velocity=self.linear_velocity)')
        self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_far_offset,
                                 velocity=self.linear_velocity)
        # safe height engage
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.wall_grid.locations[wall_index] + Location(z=4),velocity=self.linear_velocity)')
        self.ur.move_to_location(self.wall_grid.locations[wall_index] + Location(z=2), velocity=self.linear_velocity)
        # engage
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.wall_grid.locations[wall_index],velocity=self.linear_velocity)')
        self.ur.move_to_location(self.wall_grid.locations[wall_index], velocity=self.linear_velocity)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.open_gripper(0)')
        self.ur.open_gripper(0)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_close_offset,velocity=self.linear_velocity)')
        self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_far_offset,
                                 velocity=self.linear_velocity)
        # approach far
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_far_offset,velocity=self.linear_velocity)')
        self.ur.move_to_location(self.quantos_sequence.locations['wall_sh'] + self.wall_approach_far_offset,
                                 velocity=self.linear_velocity)

    def pickup_gun_from_wall(self, wall_index: str):
        # approach close
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_close_offset)')
        self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_far_offset)
        # safe height engage

        # engage
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.wall_grid.locations[wall_index], velocity=self.linear_velocity)')
        self.ur.move_to_location(self.wall_grid.locations[wall_index], velocity=self.linear_velocity)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.open_gripper(0.5)')
        self.ur.open_gripper(0.6)
        self.ur.move_to_location(self.wall_grid.locations[wall_index] + Location(z=2), velocity=10)
        # safe height engage
        # approach close
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_close_offset, velocity=20, acceleration=100)')
        self.ur.move_to_location(self.wall_grid.locations[wall_index] + self.wall_approach_far_offset, velocity=20,
                                 acceleration=100)
        # self.ur.move_to_location(self.quantos_sequence['wall_sh'], velocity=self.linear_velocity)

    def place_gun_in_quantos(self, wall_index: str):
        # open quantos door and lower the z stage
        # self.quantos.home_z_stage()
        # self.quantos.move_z_stage(3000)
        self.quantos.front_door_position = "open"
        # self.ur.move_to_location(self.quantos_sequence.locations['wall_sh'], velocity=self.linear_velocity)

        self.ur.move_joints(self.quantos_sequence.joints['home'], velocity=self.joint_velocity)

        self.ur.move_joints(self.quantos_sequence.joints['gun_mid'], velocity=self.joint_velocity)
        # ur_deck.arm.move_joints(sequence1.joints['quan_engage'],velocity=20,acceleration=40)

        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_joints(self.quantos_sequence.jointsgun_out, velocity=10)')
        self.ur.move_joints(self.quantos_sequence.joints['gun_out'], velocity=20)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.quantos_sequence.locationsquan_engage+Location(z=3), velocity=20)')
        self.ur.move_to_location(self.quantos_sequence.locations['gun_engage'] + Location(z=2), velocity=20)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_to_location(self.quantos_sequence.locationsgun_engage, velocity=self.linear_velocity)')
        self.ur.move_to_location(self.quantos_sequence.locations['gun_engage'], velocity=20)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.open_gripper(0)')
        time.sleep(1)
        self.ur.open_gripper(0)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_joints(self.quantos_sequence.jointsgun_out, velocity=10)')
        self.ur.move_to_location(self.quantos_sequence.locations['gun_out'], velocity=20)

        self.ur.move_joints(self.quantos_sequence.joints['gun_mid'], velocity=10)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=wall_index,
                               command='self.ur.move_joints(self.quantos_sequence.jointsWaypoint_9, velocity=self.joint_velocity)')
        self.ur.move_joints(self.quantos_sequence.joints['home'], velocity=self.joint_velocity)

    def current_solid_name(self):
        # read what gun is inserted
        self.quantos.lock_dosing_pin_position()
        solid_info = str(self.quantos.head_data).split()
        print(solid_info)
        solid_name = ' '.join(solid_info[4:])
        time.sleep(2)
        self.quantos.unlock_dosing_pin_position()
        # print(solid_info)
        # solid_name = solid_info[-1]
        print(solid_name)
        return solid_name

    def pickup_gun_from_quantos(self):
        solid_name = self.current_solid_name()
        wall_index = deck_consumables.get_solid_info(name=solid_name)

        time.sleep(1)
        # open quantos door and lower the z stage
        self.quantos.home_z_stage()
        self.quantos.front_door_position = "open"
        # unscrew the gun
        self.quantos.unlock_dosing_pin_position()
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=solid_name,
                               command='self.ur.move_joints(self.quantos_sequence.jointsapproach_wall, velocity=self.joint_velocity)')

        self.ur.move_joints(self.quantos_sequence.joints['home'], velocity=self.joint_velocity)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=solid_name,
                               command='self.ur.open_gripper(0)')
        self.ur.open_gripper(0)

        self.ur.move_joints(self.quantos_sequence.joints['gun_mid'], velocity=self.joint_velocity)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=solid_name,
                               command='self.ur.move_to_location(self.quantos_sequence.locationsgun_engage, velocity=self.linear_velocity)')
        self.ur.move_joints(self.quantos_sequence.joints['gun_out'], velocity=20)
        self.ur.move_to_location(self.quantos_sequence.locations['gun_engage'], velocity=self.linear_velocity)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=solid_name,
                               command='self.ur.open_gripper(0.5,velocity=0)')
        self.ur.open_gripper(0.55, velocity=0)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=solid_name,
                               command='self.ur.move_to_location(self.quantos_sequence.locationsquan_engage+Location(z=3), velocity=20)')
        self.ur.move_to_location(self.quantos_sequence.locations['gun_engage'] + Location(z=2), velocity=20)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=solid_name,
                               command='self.ur.move_joints(self.quantos_sequence.jointsgun_out, velocity=10)')
        self.ur.move_to_location(self.quantos_sequence.locations['gun_out'], velocity=20)

        self.ur.move_joints(self.quantos_sequence.joints['gun_mid'], velocity=10)
        self.write_dict_to_csv(time_stmp=datetime.datetime.now(), exp_name=solid_name,
                               command='self.ur.move_joints(self.quantos_sequence.jointsWaypoint_9, velocity=self.joint_velocity)')

        self.ur.move_joints(self.quantos_sequence.joints['home'], velocity=self.joint_velocity)
        self.ur.move_joints(self.quantos_sequence.joints['wall_sh'], velocity=self.joint_velocity)
        return wall_index

    def switch_gun(self, solid_name: str):
        wall_index = self.pickup_gun_from_quantos()
        self.place_gun_on_wall(wall_index=wall_index)
        self.pickup_gun_from_wall(wall_index=deck_consumables.get_solid_info(name=solid_name))
        self.place_gun_in_quantos(wall_index=wall_index)


if __name__ == "__main__":
    # arm
    UR_arm = UR3Arm('192.168.254.88')
    arm = UR_arm
    quan = ArduinoAugmentedQuantos('192.168.254.2', 13, logging_level=10)
    wall_grid = StaticGrid([Location(x=-328.6, y=360.1, z=359.3, rx=65.15, ry=-0.003652, rz=-90.01),
                            Location(x=-326.5, y=410.7, z=357.5, rx=65.15, ry=0.00338, rz=-90.01),
                            Location(x=-327.7, y=360.0, z=451.0, rx=65.16, ry=0.008699, rz=-90.01),
                            Location(x=-325.4, y=410.8, z=448.4, rx=65.16, ry=0.005148, rz=-90.02),
                            Location(x=-327.1, y=360.0, z=541.7, rx=65.16, ry=0.009651, rz=-90.01),
                            Location(x=-323.8, y=410.0, z=540.0, rx=65.15, ry=0.002098, rz=-90.01)],
                           columns=2, rows=3)
    qg = QuantosGuns(arm, quantos=quan, wall_grid=wall_grid,
                     wall_sequence_file='../../../UR/configuration/sequences/indexes.script',
                     quantos_sequence_file='../../../UR/configuration/sequences/quantos.script')
    for i in range(3):
        qg.switch_gun('GENTISIC')
        qg.switch_gun('salicylic acid')
        qg.switch_gun('ICE')

