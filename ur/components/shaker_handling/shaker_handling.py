import time
from ika import Thermoshaker
from ur.components.hplc_handling.hplc_handling import HplcHandling

class ShakerHandling:
    def __init__(self, ts: Thermoshaker, hplc_handling: HplcHandling):
        self.ts = ts
        self.hplc_handling = hplc_handling

    def temper_over_time(self, start_temp: float, end_temp: float, time_min: int):
        start_time = time.time()
        self.ts.set_temperature = start_temp
        self.ts.start_tempering()
        temp_diff = end_temp - start_temp
        temp_rate = temp_diff/time_min
        current_temp = start_temp
        for t in range(time_min):
            current_temp += temp_rate
            self.ts.set_temperature = current_temp
            self.ts.start_tempering()
            time.sleep(60)
        self.ts.set_temperature = end_temp
        self.ts.start_tempering()
        time_elapsed = (time.time() - start_time)/60

    def temper_over_time_with_hplc(self, start_temp: float, end_temp: float, time_min: int, hplc_runtime: float):
        start_time = time.time()
        self.ts.set_temperature = start_temp
        self.ts.start_tempering()
        temp_diff = end_temp - start_temp
        temp_rate = temp_diff / time_min
        current_temp = start_temp
        for t in range(time_min):
            self.hplc_handling.run_next_vial_in_queue_if_ready(hplc_runtime=hplc_runtime)
            current_temp += temp_rate
            self.ts.set_temperature = current_temp
            self.ts.start_tempering()
            time.sleep(60)
        self.ts.set_temperature = end_temp
        self.ts.start_tempering()

    def shake_with_hplc(self, shake_time: int, hplc_runtime: float):
        end_time = time.time() + (shake_time * 60)
        while time.time() < end_time:
            self.hplc_handling.run_next_vial_in_queue_if_ready(hplc_runtime=hplc_runtime)
            time.sleep(5)
            time_remaining_min = float((end_time - time.time())/60)
            print(f'{time_remaining_min} minutes remaining')

if __name__ == "__main__":
    port = 'COM17'  # todo set to the correct port
    dummy = False  # todo set to true if testing without connecting to an actual thermoshaker
    kwargs = {
        'port': port,
        'dummy': dummy,
    }
    ts = Thermoshaker.create(**kwargs)
    shaker_handling = ShakerHandling(ts)
    time_elapsed = shaker_handling.temper_over_time(start_temp=40, end_temp=20, time_min=10)
    print(time_elapsed)


