from numpy import disp
#from samplomatic_stepper.samplomatic_stepper import SamplomaticStepper
from ur.components.samplomatic_stepper.samplomatic_stepper import SamplomaticStepper
from north_devices.pumps.tecan_cavro import TecanCavro
from ftdi_serial import Serial
import time
#import seriali

class Samplomatic():
    def __init__(self, somatic_stepper_port,  tecan_adress=0, port_to_prime_from=2, somatic_port=1, empty_port=3, tecan_cavro_port=None):
        self.port_to_prime_from = port_to_prime_from
        self.somatic_port = somatic_port
        self.empty_port = empty_port
        serial_cavro = Serial(device_serial='FT3FJFPV', baudrate=9600)
        self.pump = TecanCavro(serial=serial_cavro, address=tecan_adress, syringe_volume_ml=2.5)
        self.pump.home()
        self.volume_inside = 0
        self.needle = self.setup_needle(somatic_stepper_port)


    def setup_needle(self, somatic_stepper_port):
        needle = SamplomaticStepper(port=somatic_stepper_port)
        # print(f"{needle=} initialized.")
        return needle

    def get_volume(self):
        return self.volume_inside

    def prime(self, volume_ml = 2):

        self.needle.move_in_mm(distance=30)
        self.pump.dispense_ml(volume_ml=volume_ml, from_port=self.port_to_prime_from, to_port=self.somatic_port,
                              velocity_ml=8.0, dispense_velocity_ml=8.0)

        self.needle.move_in_mm_up(distance=30)

        self.volume_inside = 0


    def rinse(self, port_to_rinse_from=None,  volume_ml: int = 1, distance: int = 30):
        if port_to_rinse_from == None:
            port_to_rinse_from = self.port_to_prime_from

        self.needle.move_in_mm(distance=distance)
        self.pump.dispense_ml(volume_ml=volume_ml, from_port=port_to_rinse_from, to_port=self.somatic_port, velocity_ml=8.0, dispense_velocity_ml=8.0)
        self.needle.move_in_mm_up(distance=distance)
        self.get_airgap()

    def home_needle(self):
        self.needle.home()

    # def get_airgap(self, volume_airgap_mL=0.03):
    #     # for i in range(2):
    #     move_back_vol = 0.100-volume_airgap_mL
    #     self.pump.dispense_ml(volume_ml=0.100, from_port=self.somatic_port, to_port=self.port_to_prime_from, velocity_ml=2, dispense_velocity_ml=5)
    #     time.sleep(0.25)
    #     self.pump.dispense_ml(volume_ml=move_back_vol, from_port=self.port_to_prime_from, to_port=self.somatic_port, velocity_ml=5, dispense_velocity_ml=5)

    def fill_line_with_air(self, volume_ml: int = 1.2):
        self.pump.dispense_ml(volume_ml=volume_ml, from_port = self.somatic_port, to_port=self.empty_port, velocity_ml = 5)

    def get_airgap(self, volume_ml: float = 0.03, pump_velocity: int = 2):
        self.pump.switch_valve(position=1)
        self.pump.move_relative_ml(delta_ml=volume_ml, velocity_ml=pump_velocity)
        self.volume_inside += volume_ml

    def aspirate_sample(self, volume_ml, vial_depth, pump_velocity: int = 5):
        self.pump.switch_valve(position = 1)
        self.needle.move_in_mm(distance=vial_depth)
        self.pump.move_relative_ml(delta_ml=volume_ml, velocity_ml=pump_velocity)
        self.needle.move_in_mm_up(distance=vial_depth)
        self.volume_inside += volume_ml

    def move_needle_down(self, vial_depth):
        self.needle.move_in_mm(distance=vial_depth)

    def move_needle_up(self, vial_depth):
        self.needle.move_in_mm_up(distance=vial_depth)

    def dispense_backend_solvent(self, vial_depth, volume_ml, pump_velocity: int = 5):
        self.needle.move_in_mm(distance=vial_depth)
        self.pump.dispense_ml(volume_ml=volume_ml, from_port=self.port_to_prime_from, to_port=self.somatic_port, velocity_ml=8.0, dispense_velocity_ml=8.0)
        self.needle.move_in_mm_up(distance=vial_depth)

    def dispense_sample(self,  vial_depth, volume_ml, pump_velocity: int = 5):
        self.pump.switch_valve(position=1)
        if volume_ml == None:
            volume_ml = self.volume_inside
            vol_to_remain = 0

        # if dispense_vol_ml > self.volume_inside:
        #     raise(f"Cannot dispense {dispense_vol_ml} mL since only {self.volume_inside} mL are available.")
        else:
            vol_to_remain = self.volume_inside-volume_ml

        self.needle.move_in_mm(distance=vial_depth)
        self.pump.move_absolute_ml(position_ml=vol_to_remain, velocity_ml=pump_velocity)
        self.needle.move_in_mm_up(distance=vial_depth)
        self.volume_inside -= volume_ml

def main():
    print("in main")
    samplomatic = Samplomatic("COM9", tecan_adress=0, port_to_prime_from=2,
                              somatic_port=1, empty_port=3)
    samplomatic.home_needle()
    samplomatic.fill_line_with_air()

    # somatic.move_needle_down(vial_depth=25)

    # arm.disconnect()
    # somatic.disconnet()
    # somatic.prime(volume_ml=2.5
    # somatic.pump.command_request_raw("IA0")
    #omatic.pump.dispense_ml(volume_ml= 1, from_port= 3, to_port= 2, velocity_ml=6, dispense_velocity_ml=18,  wait=True, execute=True)
    # somatic.rinse()
    
    # stepper = SamplomaticStepper(11)
    # somatic.needle.home()
    # somatic.aspirate_sample( volume_ml=0.5, vial_type="hplc_thin")
    # somatic.dispense_sample(vial_type="hplc_thin")



if __name__ == "__main__":
    main()