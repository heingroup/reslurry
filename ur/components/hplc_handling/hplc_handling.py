import time
from kinova.configuration.arm_manager.arm_manager import ArmManager
from ur.components.vial_handling.vial_handling import VialHandling
from ur.components.hplc_comm.hplc_comm import HPLC_Comm

class HplcHandling:
    def __init__(self, kinova_arm:ArmManager, vial_handling: VialHandling, hplc_comm:HPLC_Comm):
        self.vial_handling = vial_handling
        # self.hplc_relay = hplc_relay
        self.arm_manager = kinova_arm
        self.hplc_comm = hplc_comm
        self.target_time = 0
        self.run_number = 0
        self.vial_queue = []
        self.prev_runs = []
        self.first_run = True
        self.hplc_ready = False

    #todo make a way to save hplc run number to a dictionary with vial index

    #use this fucntion to prioritize running a new sample ASAP, script will wait for HPLC run to finish
    def wait_for_timer(self):
        while time.time() < self.target_time:
            mins, secs = divmod(round(self.target_time - time.time()), 60)
            timer = 'HPLC finishes in {:02d}:{:02d}'.format(mins, secs)
            print(timer + ', waiting for completion')
            time.sleep(10)

    # use this function to prioritize making samples before moving them, script will pass if run is not done
    def check_for_timer(self):
        if time.time() < self.target_time:
            mins, secs = divmod(round(self.target_time - time.time()), 60)
            timer = 'HPLC finishes in {:02d}:{:02d}'.format(mins, secs)
            print(timer + ', making new sample')
            return False
        elif time.time() >= self.target_time:
            print('HPLC complete, starting new run')
            return True

    # this function will wait for completion of the run, then submit the vial the was most recently made
    def run_current_vial(self, vial_index: str = 'A1', hplc_runtime: float = 10):
        self.vial_handling.vial_from_tray(vial_index=vial_index)
        self.vial_handling.vial_to_handoff(index='front')
        self.arm_manager.vial_hplc_exchange()
        if not self.first_run:
            self.wait_for_timer()
        # self.hplc_relay.write(b"r"
        self.hplc_comm.send_tcp_message('start')
        self.run_number += 1
        self.target_time = time.time() + hplc_runtime * 60
        print(vial_index + ' is HPLC run ' + str(self.run_number))
        self.prev_runs.append(vial_index)
        if not self.first_run:
            self.vial_handling.vial_from_handoff(index='back')
            self.vial_handling.vial_to_tray(vial_index=self.prev_runs[self.run_number - 2])
        else:
            self.first_run = False
            self.vial_handling.home()

    def return_last_vial(self):
        self.wait_for_timer()
        self.vial_handling.home()
        self.arm_manager.vial_hplc_exchange()
        self.arm_manager.hplc_reset_to_default()
        self.first_run = True
        self.vial_handling.vial_from_handoff(index='back')
        self.vial_handling.home()
        self.vial_handling.vial_to_tray(vial_index=self.prev_runs[self.run_number - 1])

    def return_last_vial_leo_mod(self):
        self.arm_manager.vial_hplc_exchange()
        self.arm_manager.hplc_reset_to_default()
        self.first_run = True
        self.vial_handling.vial_from_handoff(index='back')
        self.vial_handling.home()
        self.vial_handling.vial_to_tray(vial_index=self.prev_runs[self.run_number - 1])

    def add_to_queue_and_run_if_ready(self, vial_index: str = 'A1', hplc_runtime: float = 10):
        self.vial_queue.append(vial_index)
        print(f'current queue: {self.vial_queue}')
        if not self.first_run:
            self.hplc_ready = self.check_for_timer()
        if self.hplc_ready or self.first_run:
            current_vial_index = self.vial_queue.pop(0)
            self.vial_handling.vial_from_tray(vial_index=current_vial_index)
            self.vial_handling.vial_to_handoff(index='front')
            self.arm_manager.vial_hplc_exchange()
            # self.hplc_relay.write(b"r")
            self.hplc_comm.send_tcp_message('start')
            self.hplc_ready = False
            self.run_number += 1
            self.target_time = time.time() + hplc_runtime * 60
            print(current_vial_index + ' is HPLC run ' + str(self.run_number))
            self.prev_runs.append(current_vial_index)
            if not self.first_run:
                self.vial_handling.vial_from_handoff(index='back')
                self.vial_handling.vial_to_tray(vial_index=self.prev_runs[self.run_number - 2])
            else:
                self.first_run = False
                self.vial_handling.home()

    def run_next_vial_in_queue_if_ready(self, hplc_runtime:float = 10):
        self.hplc_ready = self.check_for_timer()
        if not self.vial_queue:
            return
        if self.hplc_ready:
            current_vial_index = self.vial_queue.pop(0)
            self.vial_handling.vial_from_tray(vial_index=current_vial_index)
            self.vial_handling.vial_to_handoff(index='front')
            self.arm_manager.vial_hplc_exchange()
            # self.hplc_relay.write(b"r"
            self.hplc_comm.send_tcp_message('start')
            self.hplc_ready = False
            self.run_number += 1
            self.target_time = time.time() + hplc_runtime * 60
            print(current_vial_index + ' is HPLC run ' + str(self.run_number))
            self.prev_runs.append(current_vial_index)
            self.vial_handling.vial_from_handoff(index='back')
            self.vial_handling.vial_to_tray(vial_index=self.prev_runs[self.run_number - 2])

    def run_remainder_in_queue(self, hplc_runtime:float = 10):
        for i in range(len(self.vial_queue)):
            self.wait_for_timer()
            vial_index = self.vial_queue.pop(0)
            self.vial_handling.vial_from_tray(vial_index=vial_index)
            self.vial_handling.vial_to_handoff(index='front')
            self.arm_manager.vial_hplc_exchange()
            # self.hplc_relay.write(b"r")
            self.hplc_comm.send_tcp_message('start')
            self.run_number += 1
            self.target_time = time.time() + hplc_runtime * 60
            print(vial_index + ' is HPLC run ' + str(self.run_number))
            self.prev_runs.append(vial_index)
            self.vial_handling.vial_from_handoff(index='back')
            self.vial_handling.vial_to_tray(vial_index=self.prev_runs[self.run_number - 2])

    def run(self, vial_sequence, hplc_runtime:float):
        for i in range(len(vial_sequence)):
            self.vial_handling.vial_from_tray(vial_index=vial_sequence[i])
            self.vial_handling.vial_to_handoff(index='front')
            if i !=0:
                self.start_timer(target_time)
            self.arm_manager.vial_hplc_exchange()
            # self.hplc_relay.write(b"r")
            self.hplc_comm.send_tcp_message('start')
            target_time = time.time() + hplc_runtime * 60
            if i !=0:
                self.vial_handling.vial_from_handoff(index='back')
                self.vial_handling.vial_to_tray(vial_index=vial_sequence[i-1])
        self.start_timer(target_time)
        self.arm_manager.vial_hplc_exchange()
        self.vial_handling.vial_from_handoff(index='back')
        self.vial_handling.vial_to_tray(vial_index=vial_sequence[-1])

    def start_timer(self, target_time):
        while time.time() < target_time:
            mins, secs = divmod(round(target_time - time.time()), 60)
            timer = 'HPLC finishes in {:02d}:{:02d}'.format(mins, secs)
            print(timer, end="\r")
            time.sleep(10)
        print("HPLC: finished")

    def run_blank(self):
        # self.hplc_relay.write(b"r")
        self.hplc_comm.send_tcp_message('start')
        self.target_time = time.time() + 60

    def process_data_and_send_csv(self):
        self.hplc_comm.send_tcp_message('get_data')


if __name__ == '__main__':
    import time
    from ur.configuration.ur_deck import vial_handling, kinova, hplc_comm


    hplc_handling = HplcHandling(kinova_arm=kinova, vial_handling=vial_handling, hplc_comm=hplc_comm)

    # for i in range(5):
    #     hplc_handling.run_blank()
    #     time.sleep(15)
    # hplc_handling.process_data_and_send_csv()
    # # for i in range(10):
    # #     hplc_handling.run_blank()

    vials = ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']

    for v in range(len(vials)):
        hplc_handling.run_current_vial(vial_index=vials[v], hplc_runtime=1)