import time

from hein_robots.robotics import Location
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from ika import MagneticStirrer


class StirHandling:

    def __init__(self, ur: UR3Arm, stirrer: MagneticStirrer, stirrer_sequence_file: str,
                 joint_velocity: float = 60, linear_velocity: float = 80):
        self.ur = ur
        self.stirrer = stirrer
        self.stirrer_sequence = URScriptSequence(stirrer_sequence_file)
        self.joint_velocity = joint_velocity
        self.linear_velocity = linear_velocity

    def tube_to_stir(self, vial_type:str='centrifuge'):
        self.ur.move_to_location(self.stirrer_sequence.locations['home'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.stirrer_sequence.locations['hplc_stir'] + Location(z=90), velocity=self.linear_velocity)
        if vial_type == 'hplc':
            self.ur.move_to_location(self.stirrer_sequence.locations['hplc_stir'], velocity=self.linear_velocity)
        else:
            self.ur.move_to_location(self.stirrer_sequence.locations['hplc_stir'] + Location(z=11), velocity=self.linear_velocity)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(self.stirrer_sequence.locations['hplc_stir'] + Location(z=90), velocity=self.linear_velocity)


    def tube_from_stir(self, vial_type:str='centrifuge'):
        self.ur.move_to_location(self.stirrer_sequence.locations['home'], velocity=self.linear_velocity)
        self.ur.close_gripper(0.6)
        self.ur.move_to_location(self.stirrer_sequence.locations['hplc_stir'] + Location(z=90), velocity=self.linear_velocity)
        if vial_type == 'hplc':
            self.ur.move_to_location(self.stirrer_sequence.locations['hplc_stir'], velocity=self.linear_velocity)
        else:
            self.ur.move_to_location(self.stirrer_sequence.locations['hplc_stir'] + Location(z=11), velocity=self.linear_velocity)
        self.ur.close_gripper(0.85)
        self.ur.move_to_location(self.stirrer_sequence.locations['hplc_stir'] + Location(z=90), velocity=self.linear_velocity)
        self.ur.move_to_location(self.stirrer_sequence.locations['home'], velocity=self.linear_velocity)


    def run_stir(self, stir_duration:float, speed:float=100, vial_type:str='centrifuge'):
        """

        :param stir_duration: in s
        :param speed: in rpm
        :return: whether dissolved TODO
        """
        self.tube_to_stir(vial_type=vial_type)
        self.stirrer.stirring = speed
        self.stirrer.start_stirring()
        time.sleep(stir_duration)
        self.stirrer.stop_stirring()
        dissolved = True #todo
        self.tube_from_stir(vial_type=vial_type)
        return dissolved

    def stir_for_duration(self,stir_duration:float, speed:float=250):
        self.stirrer.stirring = speed
        self.stirrer.set_speed_value(speed)
        self.stirrer.start_stirring()
        time.sleep(stir_duration)
        self.stirrer.stop_stirring()

    def stir_forever(self, speed: float = 250):
        self.stirrer.stirring = speed
        self.stirrer.set_speed_value(speed)
        self.stirrer.start_stirring()
