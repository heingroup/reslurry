import socket
import pyautogui
import time
from file_sorter import FolderManager
from peak_picker import HPLC_Picker
from datetime import date

class UR_Comm:
    def __init__(self, peak_picker: HPLC_Picker, fm: FolderManager,  ip, port, solid_name):
        self.peak_picker = peak_picker
        self.fm = fm
        self.ip = ip
        self.port = port
        self.share_folder = '//HEINLAB32/summary_data'
        self.solid_name = solid_name

    def listen_for_message(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.ip, self.port))
            s.listen()
            while True:
                conn, addr = s.accept()
                print(f'Connection from {addr} established')
                while True:
                    print('Received message from', addr)
                    data = conn.recv(1024).decode().strip()
                    print(data)
                    if data == 'start':
                        self.resume_queue()
                    elif data == 'get_data':
                        self.fm.copy_folders()
                        self.peak_picker.find_peaks_in_sequence()
                        self.peak_picker.convert_to_csv()
                    # elif "set_sequence_table" in data:
                    #     self.sequence_table_auto(data)
                    #will add after it's working well
                    else:
                        self.set_up_queue(data)
                        self.fm.create_new_folder(solid_name=self.solid_name, experiment_type=data)
                        combined_folder = fm.return_target_folder()
                        self.peak_picker.assign_combined_folder(combined_folder)
                        start_date = date.today().strftime("%Y-%m-%d")
                        csv_name = start_date + "-" + self.solid_name + "-" + data + ".csv"
                        self.peak_picker.assign_csv_name(csv_name)

                    conn.close()
                    break
                time.sleep(5)  # wait for 5 seconds before continuing to listen for messages

    def close_socket(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.close()

    def resume_queue(self):
        locations = [[371, 137], [397, 163], [430, 189], [460, 215], [474, 242], [497, 270], [525, 292], [551, 319], [584, 346]]
        for location in locations:
            pyautogui.click(location[0], location[1])
            time.sleep(0.1)

    def set_up_queue(self, experiment):
        file_name = experiment + '.qpl'
        locations = [[61, 28], [120, 270], [558, 658], [859, 850]]

        for location in locations:
            pyautogui.click(location)
            time.sleep(0.5)

        pyautogui.typewrite(file_name)

        queue_locations = [[1304, 880], [1003, 663], [584, 134]]

        for location in queue_locations:
            pyautogui.click(location)
            time.sleep(2)

        play_button = 'D:\git_repositories\HPLC_watcher\play_button.png'

        button_location = None
        for i in range(20):
            button_location = pyautogui.locateCenterOnScreen(play_button, confidence=0.95)
        pyautogui.moveTo(button_location)
        time.sleep(0.5)
        pyautogui.click(button_location)






if __name__ == '__main__':
    # WHEN RUNNING AUTOMATED SEQUENCES, YOU MUST HAVE THE THE FOLDER IN THIS FORMAT:
    # auto

    #CHANGE SO IT JUST GRABS FROM NAME "auto"

    #Change name of solid and experiment type
    #Options: "std_curve", "cooling_cryst", "wash"
    solid_name = 'pyrethroic acid'

    #Default parameters, do not change (consider making multiple summary files?)
    ip = '0.0.0.0'  # Listen on all network interfaces
    port = 29620

    fm = FolderManager()
    peak_picker = HPLC_Picker(solid_name=solid_name)

    ur_comm = UR_Comm(ip=ip, port=port, peak_picker=peak_picker, fm=fm, solid_name = solid_name)

    ur_comm.listen_for_message()

    #Send TCP message with name of file it just created?


