import datetime
import os
import shutil
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer
import time
import pandas as pd
import glob
from datetime import date



#Consider OS path join thing

class HPLC_Picker:
    def __init__(self, solid_name:str, tolerance = 0.3):
        self.combined_folder = str
        self.share_folder: str = '//heinlab38/users/user/PycharmProjects/self_driving_solubility/ur/experiments/hplc_data'
        self.csv_name = str
        self.tolerance = tolerance
        self.peaks = []
        self.wavelength_files = {210:'AUTOMATICALLY_GENERATED_REPORT01.csv',
                                 230:'AUTOMATICALLY_GENERATED_REPORT02.csv',
                                 254:'AUTOMATICALLY_GENERATED_REPORT03.csv',
                                 290:'AUTOMATICALLY_GENERATED_REPORT04.csv',
                                 240: 'AUTOMATICALLY_GENERATED_REPORT04.csv',
                                 270:'AUTOMATICALLY_GENERATED_REPORT05.csv'}
        self.summary_df = pd.DataFrame
        self.solid_name = solid_name

    def assign_combined_folder(self, combined_folder):
        self.combined_folder = combined_folder

    def assign_csv_name(self, csv_name):
        self.csv_name = csv_name

    def assign_peak(self, label, retention_time, wavelength):
        peak_assign_dict = {'label':label, 'rt':retention_time, 'wavelength':wavelength}
        self.peaks.append(peak_assign_dict)

    def create_summary_df(self):
        labels = ['sample']
        for peak in self.peaks:
            labels.append(peak['label'])
        self.summary_df = pd.DataFrame(columns = labels)

    def print_dict(self):
        print(self.peaks)

    def print_df(self):
        print(self.summary_df)

    def convert_to_csv(self):
        set_date = date.today().strftime("%Y-%m-%d")
        share_folder = self.share_folder + '/' + set_date
        if not os.path.exists(share_folder):
            os.makedirs(share_folder)

        base_name, extension = os.path.splitext(self.csv_name)
        destination_path = os.path.join(share_folder, self.csv_name)
        counter = 1

        while os.path.exists(destination_path):
            # Generate a new file name with an integer suffix
            new_name = f"{base_name}_{counter}{extension}"
            destination_path = os.path.join(share_folder, new_name)
            counter += 1

        self.summary_df.to_csv(destination_path)

    def find_peaks_in_one_run(self, run_folder):

        peak_dict = {'sample':run_folder}

        for peak in self.peaks:
            current_wavelength = peak['wavelength']
            peak_file = os.path.join(run_folder, self.wavelength_files[current_wavelength])
            if not os.path.exists(peak_file):
                peak_dict[peak['label']] = 0
            else:
                df = pd.read_csv(peak_file, names=['number','rt','type','width','area','height','percent'],
                                 encoding="utf-16", sep=',')
                rt_min = peak['rt'] - self.tolerance
                rt_max = peak['rt'] + self.tolerance
                df = df[(df['rt'] >= rt_min) & (df['rt'] <= rt_max)]
                if df.empty:
                    peak_dict[peak['label']] = 0
                else:
                    peak_dict[peak['label']] = df['area'].item()

        self.summary_df = pd.concat([self.summary_df, pd.DataFrame.from_records([peak_dict])])

    def find_peaks_in_sequence(self):
        self.assign_all_peaks(self.solid_name)
        while True:
            try:
                self.create_summary_df()
                runs = glob.glob(self.combined_folder + '/*.D')
                runs.sort(key=lambda x: os.path.getmtime(x))
                for run_folder in runs:
                    self.find_peaks_in_one_run(run_folder=run_folder)
                self.peaks = []
                break
            except ValueError as e:
                if str(e) == "can only convert an array of size 1 to a Python scalar":
                    self.tolerance -= 0.005
                    continue


    # Modify this function with the retention times, names and wavelengths of your solid components
    def assign_all_peaks(self, solid_name):
        if solid_name == 'dasabuvir':
            self.assign_peak('N1', 1.7, 254)
            self.assign_peak('A', 1.48, 230)
            self.assign_peak('N3', 1.52, 230)
            self.assign_peak('B', 2.46, 230)
            self.assign_peak('C', 2.77, 230)
            self.assign_peak('D', 3.04, 230)
        elif (solid_name == 'salicylic acid') or (solid_name =='SA_mix'):
            self.assign_peak('4HBA', 1.97, 254)
            self.assign_peak('OAA', 3.15, 230)
            self.assign_peak('BA', 3.43, 230)
            self.assign_peak('SA', 3.63, 230)
            self.assign_peak('4CBA', 4.36, 254)
        elif (solid_name == 'pyrethroic acid') or (solid_name == "pyrethoic acid") or (solid_name =="ND-1-85"):
            self.assign_peak('isomer_210', 2.38, 210)
            self.assign_peak('isomer_230', 2.38, 230)
            self.assign_peak('product_210', 2.488, 210)
            self.assign_peak('product_240', 2.488, 240)
            self.assign_peak('product_254', 2.488, 254)
        elif (solid_name == 'ND-1-81') or (solid_name == "hydrazone"):
            self.assign_peak('hydrazine', 1.51, 230)
            self.assign_peak('A', 2.453, 230)
            self.assign_peak('B', 2.603, 210)
            self.assign_peak('product_290', 2.96, 290)
            self.assign_peak('product_254', 2.96, 254)
            self.assign_peak('BHT', 3.652, 230)
        elif (solid_name == 'ABT-263'):
            self.assign_peak('compound_290', 2.2, 290)
            self.assign_peak('compound_254', 2.2, 254)
        elif (solid_name == 'jacob_wetcake'):
            self.assign_peak('A', 2.17, 210)
            self.assign_peak('B', 2.54, 210)
            self.assign_peak('C', 2.79, 210)
            self.assign_peak('D', 3.29, 210)
            self.assign_peak('E', 3.70, 210)
            self.assign_peak('product', 4.85, 254)
        elif (solid_name == 'spiro'):
            self.assign_peak('ArBr', 3.03, 210)
            self.assign_peak('mono', 4.64, 210)
            self.assign_peak('di-same', 5.23, 210)
            self.assign_peak('di-opp', 6.05, 210)
            self.assign_peak('tri', 6.54, 210)




if __name__ == '__main__':
    folder = r"D:\Chemstation\1\Data\2023-09-28-pyrethroic_acid_dilution4"
    destination_file = r"D:/Chemstation/results.csv"
    solid_name = 'pyrethroic acid'
    peak_picker = HPLC_Picker(solid_name=solid_name)
    peak_picker.assign_combined_folder(folder)
    peak_picker.assign_csv_name(destination_file)
    peak_picker.find_peaks_in_sequence()
    peak_picker.convert_to_csv()


