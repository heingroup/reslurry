import os
import shutil
from datetime import date
import subprocess
import re

class FolderManager:
    def __init__(self, folder_path: str = 'folder', target_folder_path: str = 'folder'):
        self.folder_path = folder_path
        self.target_folder_path = target_folder_path
        self.source_path: str

    def copy_folders(self):
        file_list = []
        pattern = r"^001-P1-(F4|F6)-.+\.D$"
        for dir_name in os.listdir(self.folder_path):
            if dir_name.startswith("ND_F4") or dir_name.startswith("ND_F6"):
                dir_path = os.path.join(self.folder_path, dir_name)
                for file_name in os.listdir(dir_path):
                    # Check if the file_name matches the pattern
                    if re.match(pattern, file_name):
                        file_path = os.path.join(dir_path, file_name)
                        file_list.append((file_path, os.path.getctime(file_path)))
        file_list.sort(key=lambda x: x[1])  # sort the list by creation time
        for index, (file_path, _) in enumerate(file_list):
            file_name = os.path.basename(file_path)
            target_folder_path = os.path.join(self.folder_path, self.target_folder_path)
            os.makedirs(target_folder_path, exist_ok=True)
            target_file_path = os.path.join(target_folder_path, f"run_{index+1}.D")  # Include the index in the filename to avoid overwriting files with the same sample number
            shutil.copytree(file_path, target_file_path)  # use shutil.copy() to copy individual files
            print(f"Copied file {file_name} to {target_file_path}")
        self.delete_folders(folder = self.folder_path)

    def create_new_folder(self, solid_name: str, experiment_type: str, custom_date: str = "None"):
        if custom_date == "None":
            folder_date = date.today().strftime("%Y-%m-%d")
        else:
            folder_date = custom_date
        folder_index = 1
        folder_path = "D:/Chemstation/1/Data/auto"
        target_folder_path = "D:/Chemstation/1/Data/" + folder_date + "-" + solid_name + "-" + experiment_type + "-" \
                             + str(folder_index)
        while os.path.exists(target_folder_path):
            folder_index += 1
            target_folder_path = "D:/Chemstation/1/Data/" + folder_date + "-" + solid_name + "-" + experiment_type + "-" + str(folder_index)
        os.makedirs(target_folder_path)
        self.folder_path = folder_path
        self.target_folder_path = target_folder_path

    def return_target_folder(self):
        return self.target_folder_path

    def delete_folders(self, folder):
        for folder_name in os.listdir(folder):
            folder_path = os.path.join(folder, folder_name)
            if os.path.isdir(folder_path):
                shutil.rmtree(folder_path)
                print(f"Deleted folder: {folder_path}")



if __name__ == '__main__':
    source = "D:/Chemstation/results.csv"
    folder_path = "D:/Chemstation/1/Data/auto"
    target_folder_path = "D:/Chemstation/1/Data/2023-10-10-pyrethroic acid-wash_5_samples_3_duplicates-1"
    fm = FolderManager(folder_path=folder_path, target_folder_path=target_folder_path)

    fm.copy_folders()
